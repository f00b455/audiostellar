#pragma once

#include "ofMain.h"
#include "ofxImGui.h"
#include "../Sound/Sounds.h"
#include "../Servers/MidiServer.h"
#include "../Units/Units.h"
#include "../Utils/SessionManager.h"
#include "../ML/AudioDimensionalityReduction.h"
#include "GuiTheme.h"
#include "ofxJSON.h"
#include "../Utils/Tooltip.h"
#include "../Servers/OscServer.h"
#include "../Servers/MasterClock.h"
#include "../Sound/AudioEngine.h"
#include "../Sound/Voices.h"
#include "../Utils/Tutorial.h"
#include "../Utils/AudioSegmentationTool.h"


#define WINDOW_TOOLS_WIDTH 330
#define WINDOW_TOOLS_HEIGHT 600

#define WINDOW_UNITS_WIDTH 350
#define WINDOW_UNITS_HEIGHT 600

#define VERSION "1.4"

class SessionManager;
class MidiServer;

class Gui {

private:
    static Gui* instance;
    Gui();

    bool drawGui = true;

    #ifndef TARGET_OS_MAC
    bool TARGET_OS_MAC = false;
    #endif

    ofxJSONElement jsonFile;

    //this logic could probably be better
    bool isClusterContextMenuFirstOpen = false;

    bool haveToDrawWelcomeScreen = false;
    bool haveToDrawLoadingSamplesScreen = false;
    bool isPreloadingFiles = false;
    bool haveToDrawTutorial = false;
    bool haveToDrawAboutScreen = false;
    bool haveToDrawShortcutScreen = false;
    bool haveToDrawDimReductScreen = false;
    bool haveToDrawAudioSettingsScreen = false;
    bool haveToDrawClusteringSettingsScreen = false;
    bool haveToDrawPerformanceSettingsScreen = false;
    bool haveToDrawSoundContextMenu = false;
    bool haveToDrawClusterContextMenu = false;
    bool haveToDrawMessage = false;
    bool haveToDrawCrashMessage = false;
    bool haveToDrawStopRecordingScreen = false;
    bool haveToDrawFPS = true;
    bool haveToHideCursor = false;
    bool haveToDrawMidiSettingsScreen = false;
    bool haveToDrawOscSettingsScreen = false;
    bool haveToDrawMIDILearnScreen = false;
    bool haveToDrawOSCLearnScreen = false;
    bool haveToDrawSegmentationTool = false;
    bool showMIDIOSCMonitor = true;
    bool haveToDrawAbletonLinkSettingsScreen = false;
    bool haveToDrawMarkerContextMenu = false;
    bool haveToDrawFilesNotFoundMessage = false;
    bool haveToDrawMapIsDone = false;
    bool haveToFocusOnCluster = true;

    bool haveToDrawExitMessage = false;
    bool userWannaExit = false;

    // i need to know which parameter need the options displayed
    ofParameter<float> * selectedSlider = nullptr;

    bool fullscreenRequested = false;
//    bool isFullscreen = false;
    bool hideMainMenu = false;
    bool hideMainMenuWarningShown = false;
    
    string messageTitle = "";
    string messageDescription = "";
    float messageOpacity = 1.0f;
    bool messageFadeAway = false;
    
    shared_ptr<Sound> selectedSound; //for sound context window
    int selectedClusterID; // initialize
    
    AudioDimensionalityReduction adr;
    AudioSegmentationTool * ast = AudioSegmentationTool::getInstance();

    const char* sample_rate_items[4] = { "11025", "22050", "44100", "48000" };
    int sample_rate_values[4] = { 11025, 22050, 44100, 48000 };
    int sample_rate_item_current = 1;
    
    const char* window_size_items[5] = { "512", "1024", "2048", "4096", "8192" };
    int window_size_values[5] = { 512, 1024, 2048, 4096, 8192 };
    int window_size_item_current = 2;
    
    const char* hop_size_items[5] = { "Window size ", "Window size / 2", "Window size / 4", "Window size / 8", "Window size / 16" };
    int hop_size_values[5] = { 1, 2, 4, 8, 16 };
    int hop_size_item_current = 2;

    const char* viz_items[3] = { "t-SNE", "UMAP", "PCA" };
    string viz_values[5] = { "tsne", "umap", "pca" };
    int viz_item_current = 0;

    const char* metric_items[5] = { "cosine", "euclidean", "canberra", "cityblock", "braycurtis" };
    string metric_values[5] = { "cosine", "euclidean", "canberra", "cityblock", "braycurtis"  };
    int metric_item_current = 0;

    const string welcomeScreenText =
            "Commander, the default sound map has been loaded.\n\n"
            "Are you up for a fast tutorial to get you started ?\n\n";

    bool isProcessingFiles = false;

    void drawDimReductScreen();
    void drawProcessingFilesWindow();
    void drawWelcomeScreen();
    void drawExitScreen();
    void drawLoadingSamplesScreen();
    void drawShortcutsScreen();

    void drawMainMenu();
    void drawMainMenuExtra();
    void drawMainMenuCenter();
    void drawMainMenuRight();

    void drawAboutScreen();
    void drawFPS();
    void drawMessage();
    void drawCrashMessage();
    void drawStopRecordingScreen();
    void drawTutorial();
    void drawAudioSettingsScreen();
    void drawClusteringSettingsScreen();
    void drawPerformanceSettingsScreen();
    void drawContextMenu();
    void drawClusterContextMenu();
    void drawMidiSettingsScreen();
    void drawOscSettingsScreen();
    void drawMIDILearnScreen();
    void drawOSCLearnScreen();
    void drawAbletonLinkSettingsScreen();
    void drawAudioFilesNotFound();
    void drawSegmentationTool();
    void drawAudioSliceWindow();
    void drawMapIsDone();
        
    void hideIdleMouse(int idleTime); // seconds
    int previousMouseX = 0;
    int previousMouseY = 0;
    float mouseIdleStartTime = -1;

    void onControlKeyboardPressed();

    ofTrueTypeFont font;
    Tutorial tutorial;
    Tutorial::TutorialPage * tutorialCurrentPage = nullptr;
    ofTexture imgWelcome;

public:
    static Gui* getInstance();

    ofxImGui::Gui gui;

    void newSession();

    void draw();

    void keyPressed(ofKeyEventArgs & e);
    void keyReleased(ofKeyEventArgs & e);
    void mousePressed(ofVec2f p, int button);
    void mouseReleased(ofVec2f p, int button);
    void mouseScrolled(ofVec2f p, float scrollX, float scrollY);

    bool isMouseHoveringGUI();

    void showMessage(string description, string title = "Information", bool fadeAway = false);
    void hideMessage();
    void showCrashMessage();
    void showSampleLoadingScreen();
    void showExitMessage();
    void showFilesNotFoundMessage();

    void showWelcomeScreen();
    void startTutorial();

    ofFileDialogResult showSaveDialog(string defaultName, string messageName);
    ofFileDialogResult showLoadDialog(string windowTitle = "", bool bFolderSelection = false,
                                      string defaultPath = "");

    bool getShowStats();
    void setShowStats( bool v );
    bool getFullscreen();
    void setFullscreen( bool v );
    void toggleFullscreen();
    bool getShowGUI();
    void setShowGUI( bool v );
    bool getShowMIDIOSCMonitor();
    void setShowMIDIOSCMonitor( bool v );
    bool getUserWannaExit();
    bool isMouseCursorHidden();
    bool getFocusOnClusters();
    void setFocusOnClusters(bool v);

    bool isAnyModifierPressed( bool includeAlt = true );
};
