#include "AbletonLinkServer.h"
#include "../Units/Units.h"
#include <math.h>

bool AbletonLinkServer::enable = false;
AbletonLinkServer *AbletonLinkServer::instance = nullptr;

AbletonLinkServer::AbletonLinkServer()
{
    abletonLink.disable();
    ofAddListener(abletonLink.bpmChanged, this, &AbletonLinkServer::bpmChanged);
    ofAddListener(abletonLink.playStateChanged, this, &AbletonLinkServer::playStateChanged);
}

AbletonLinkServer *AbletonLinkServer::getInstance()
{
    if (instance == nullptr)
    {
        instance = new AbletonLinkServer();
    }
    return instance;
}

void AbletonLinkServer::update()
{
    if (enable && (!useExternalPlayStop || (useExternalPlayStop && playState)) )
    {
        int max = abletonLink.getQuantum() * 32;
        int pos = ceil(abletonLink.getPhase() * 4 + latency);

        if (fire == pos) {
            MasterClock::getInstance()->onAbletonLinkClockTick(tickValue);
            
            tickValue++;
            if(tickValue > max) {
                tickValue = 1;
            }
            
            fire = fire + 1;
            if(fire > 16) {
                fire = 1;
            }

//            MasterClock::getInstance()->setBPM( round(abletonLink.getBPM()) );
        }
    }
}

void AbletonLinkServer::start(bool setLinkStatus)
{
    enable = true;
    abletonLink.enable();
//    abletonLink.setPlayStateSync(true);
    MasterClock::getInstance()->setClockType(MasterClock::ABLETON_LINK);

    if ( setLinkStatus ) {
        MasterClock::getInstance()->setLinkStatus(enable);
    }
}

void AbletonLinkServer::stop(bool setLinkStatus)
{
    enable = false;
    abletonLink.disable();
    MasterClock::getInstance()->setClockType(MasterClock::INTERNAL);
    if ( setLinkStatus ) {
        MasterClock::getInstance()->setLinkStatus(enable);
    }
}

void AbletonLinkServer::drawGui()
{
    // anda prender y apagar desde el boton de arriba
    // la segunda vez que apretas checkbox se rompe
    if (ImGui::Checkbox("Enable", &enable)) {
        if ( enable ) {
            start();
        } else {
            stop();
        }
    }
    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::ABLETON_LINK);

    ImGui::Checkbox("Use external Play/Stop", &useExternalPlayStop);
    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::ABLETON_LINK_HANDLE_PLAY_STOP);

    ImGui::NewLine();

    ImGui::SliderFloat("Latency", &latency, -0.80f, 0.80f);
    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::ABLETON_LINK_LATENCY);
}

void AbletonLinkServer::bpmListener(float &v)
{
    if ( enable ) {
        abletonLink.setBPM(v);
    }
}

void AbletonLinkServer::reset()
{

}

Json::Value AbletonLinkServer::save()
{
    Json::Value val = Json::Value(Json::objectValue);

    val["enabled"] = enable;
    val["useExternalPlayStop"] = useExternalPlayStop;
    val["latency"] = latency;

    return val;
}

void AbletonLinkServer::load(Json::Value jsonData)
{
    if( jsonData != Json::nullValue ) {
        if ( jsonData["enabled"].asBool() ) {
            start();
        }

        useExternalPlayStop = jsonData["useExternalPlayStop"].asBool();
        latency = jsonData["latency"].asFloat();
    }
}


void AbletonLinkServer::bpmChanged(double &bpm)
{
//    ofLogNotice("bpmChanged bpm") << bpm;
//    MasterClock::getInstance()->setBPM((int)round(abletonLink.getBPM()));
    MasterClock::getInstance()->setBPM(bpm);
}

void AbletonLinkServer::playStateChanged(bool &b)
{
    playState = b;
}
